
 let peopleSalary  =
[
    {"id":"1001","firstname":"Luke","lastname":"Skywalker","company":"Walt Disney","salary":"40000"},
    {"id":"1002","firstname":"Tony","lastname":"Stark","company":"Marvel","salary":"1000000"},
    {"id":"1003","firstname":"Somchai","lastname":"Jaidee","company":"Love2work","salary":"20000"},
    {"id":"1004","firstname":"Monkey D","lastname":"Luffee","company":"One Piece","salary":"9000000"}

]
//หัวตาราง
function peopleHead() {
   
   let header = "<tr>"; 

    for (let key in peopleSalary[0]) {
        if (key != "company") {
            header+="<td align='center'><b>"+key+"</b></td>";
        }            
    }
    header+="</tr>"
    $('#tablepeopleSalary').append(header);
}

//ข้อมูล
function peopleDetail() {

for (i=0; i < peopleSalary.length; i++){
    let detail = "<tr>"; 
    for (let key in peopleSalary[i]) {
        if (key != "company") {
            if(key == "salary"){
                detail+="<td align='center'><ol>"
                for(let k = 0 ; k < peopleSalary[i].salary.length; k++){
                    detail+="<li>"+peopleSalary[i].salary[k]+"</li>"
                }
                detail+="</td></ol>"    
            }else{
                detail+="<td align='center'>"+peopleSalary[i][key]+"</td>";
            }
        }            
    }

        detail+="</tr>"
        $('#tablepeopleSalary').append(detail);
    }  
}

//ตั้งค่าเงินเดือน(ปี)
function setSalary() {
    for (i=0; i < peopleSalary.length; i++){
        let salaryNow = peopleSalary[i].salary;
        peopleSalary[i].salary = [];
        for(j=0; j<3; j++){
                if(!j){
                    peopleSalary[i].salary.push(salaryNow);
                }else{
                    peopleSalary[i].salary.push(peopleSalary[i].salary[j-1]*10/100+parseFloat(peopleSalary[i].salary[j-1]));
                }
        }
    }
}
setSalary();
peopleHead();
peopleDetail();


